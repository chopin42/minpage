# minpage : Minimalist static website generator.

>  This app has been created in Python and is the simplified version of Pelican.

## Installation and minimum configuration

1. Download the git repo

```bash
git clone https://gitea.com/chopin42/minpage
cd minpage
```

2. Customize the `theme/base.html` file to add your navigation menu and your site's name.

## Usage

1. Create a markdown file like `1-article.md` in the directory `content/`. The number is used to organize the articles. You can also put all the images and related content in this folder.

2. Your article needs to be organized the following way:

```markdown
{Title: Your Title here}
{Author: Your Name}
{Date: 12-12-12 12:12}
{Category: Your Category}
{Body}

You can write your *markdown* article **here**. ![image](./image.png)
```

3. Run the following command:

```shell
./basic.py
```

## Advanced configuration

You can find all the informations about the advanced configuration [here](./advanced.md).
