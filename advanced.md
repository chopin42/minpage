## Advanced configuration

### Change the style

1. Edit the file `content/style.css` to change the style of your blog.

2. Run the follwing command to apply the changes:

```shell
./basic.py
```

### Change the templates

1. The templates are located in `theme/` this is the list of the templates:
   
   * `base.html` is common to all pages, contains the header and the footer of each pages. Including the name of the website and the navigation menu.
   
   * `article.html` is displaying articles. 
   
   * `author.html` is displaying the list of articles written by a given author.
   
   * `category.html` is displaying the list of articles in a given category.
   
   * `index.html` displays all the articles.

2. To edit them, make sure to keep the same variables. The variables are like this: `{ something }` or like this `{% something else %}`.

3. Once you done that you can re-generate your website like this:

```shell
./basic.py
```

### Create new keys (ways to find articles)

> If you don't want to create a key, but just create a new tag for the articles. Skip the steps: 3, 5, 6 and 7.

1. Edit the file `basic.py`

2. Add your key in the `Article class` like this:

```python
# Define what is an article
class Article(object):
    def __init__(self, title, body, category, author, date, year): # Add your key here as well
        self.title = title
        self.body = body
        self.category = category
        self.author = author
        self.date = date
        self.year = year # Our new key
```

3. Create a dictionary for your key:

```python
articles, categories, authors, years = [], {}, {}, {}
```

4. Find your key inside the files:

```python
title = gett(fa, "{Title: ", "}")
category = gett(fa, "{Category: ", "}")
date = gett(fa, "{Date: ", "}")
author = gett(fa, "{Author: ", "}")
year = gett(fa, "{Year: }", "}") # Add this line
body = fa.split("{Body}")[1]
```

5. Assign the article to the key

```python
# Add the article into the right keys, follow the same paterns to add new keys
categories = addToKey(categories, category, article)
authors = addToKey(authors, author, article)
year = addToKey(years, year, article) # Add this line
```

6. Create the key's page

```python
# Create the lists and the keys
for article in articles:
    inject(article, "article.html", f"output/{article.title}.html")
for category in categories:
    data = [category, categories[category]]
    inject(data, "category.html", f"output/{category}.html")
for author in authors:
    data = [author, authors[author]]
    inject(data, "author.html", f"output/{author}.html")
for year in years:
    data = [year, years[year]]
    inject(year, "year.html", f"output/{year}.html")
```

7. Create a template file in the `theme/` directory. In this case: `theme/year.html`

```html
{% extends "base.html" %}

{% block content %}
<h1>All articles published the: {{ data[0] }}</h1>
<ol>
  {% for article in data[1][::-1] %}
  <li><article class="hentry">
          <header> <h2 class="entry-title"><a href="{{ article.title }}.html" rel="bookmark" title="Permalink to {{ article.title }}">{{ article.title }}</a></h2> </header>
          <footer class="post-info">
              <!-- <time class="published" datetime="{ article.date.isoformat() }}"> { article.locale_date }} </time> -->
              <address class="vcard author">By
              <a class="url fn" href="{{ article.author }}.html">{{ article.author }}</a>
              </address>
          </footer><!-- /.post-info -->
          <!-- <div class="entry-content"> {{ article.content }} </div><!-- /.entry-content --> -->
          <hr>
  </article></li>
  {% endfor %}
</ol>
{% endblock %}
```

8. You can also edit the other templates to add your key next to the article. This is an example:

```html
<a href="{{ article.year }}.html">{{ article.year }}</a>
```

9. Now, you can re-generate the website:

```bash
./basic.py
```

### Create an extension

#### For specific files

1. Edit the file `basic.py`

2. Add a new section inside the file lister:

```python
for f in os.listdir("content"):
    if f.endswith(".png"): # Add the following block of code
        # Your code here to execute with the variable "f" 
    if f.endswith(".md"):
        # Convert the file
        mdToHtml(f"content/{f}", f"content/{f.split('.md')[0] + '.html'}")
        f = f.split('.md')[0] + '.html'

        # Load the converted file
        fa = open(f"content/{f}", "r").read()
        print(f"The content of '{f}' loaded.")

        # Get the info, modify this accordingly to the definition of your article:
        title = gett(fa, "{Title: ", "}")
        category = gett(fa, "{Category: ", "}")
        date = gett(fa, "{Date: ", "}")
        author = gett(fa, "{Author: ", "}")
        body = fa.split("{Body}")[1]

        # Now create the article (modify the list accordingly with the definition of your article)
        article = Article(title, body, category, author, date)
        articles.append(article)

        # Add the article into the right keys, follow the same paterns to add new keys
        categories = addToKey(categories, category, article)
        authors = addToKey(authors, author, article)

    # if any other type of files:
    else:
        print(f"File '{f}' has been moved.")
        copyfile(f"content/{f}", f"output/{f}")
```

For example, the following code is a module to ditherize images:

```python
if f.endswith(".png") or f.endswith(".jpg"):
    from PIL import Image
    import hitherdither
    def dither(origin, output):
        size = (800, 800)
        palette = hitherdither.palette.Palette(
            [
                (25, 25, 25),
                (75, 75, 75),
                (125, 125, 125),
                (175, 175, 175),
                (225, 225, 225),
                (250, 250, 250),
            ]
        )
        threshold = [96, 96, 96]

        img = Image.open(origin)
        img.thumbnail(size, Image.LANCZOS)
        img = hitherdither.ordered.bayer.bayer_dithering(img, palette, threshold, order=8)
        if output.endswith(".jpg"):
            output = output.split(".jpg")[0] + ".png"
        img.save(output)
    dither(f"content/{f}", f"output/{f}")
    
```

3. Run the following command to apply your changes to your blog:

```shell
./basic.py
```
