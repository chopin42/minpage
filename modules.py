import os
from jinja2 import FileSystemLoader, Environment
from shutil import copyfile

def inject(data, input, to):
    env = Environment(loader=FileSystemLoader(os.path.join("theme")))
    template = env.get_template(input)
    open(to, "w").write(template.render(data=data))

def gett(stuff, pre, suff):
    return stuff.split(pre)[1].split(suff)[0]

def mdToHtml(origin, output):
    if output.endswith(".md"):
        output = output.split(".md")[0] + ".html"

    os.system(f"pandoc {origin} -o {output}")

def addToKey(d, key, article):
    try:
        d[key].append(article)
    except:
        d[key] = []
        d[key].append(article)
    return d